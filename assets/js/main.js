import '../scss/style.scss';

var Rps = function(rock,paper,scissors) {
    this.init(rock,paper,scissors);
    this.handleEvents();
};

Rps.prototype.init = function (rock,paper,scissors) {
    this.$rock = $(rock);
    this.$paper = $(paper);
    this.$scissors = $(scissors);
    this.count = 0;
    this.winNumber = 0;
    this.wh = $(window).width(); 
    this.vh = $(window).height();
    this.resizeWindow();
    this.isPlaying = false;
};

Rps.prototype.resizeWindow = function() {
    var timer = false;
    var self = this;
    $(window).resize(function() {
        console.log('test')
        if (timer !== false) {
            clearTimeout(timer);
        }
        timer = setTimeout(function() {
            console.log('resized');
            self.wh = $(window).width();
            self.vh = $(window).height();
        }, 200);
    });
}

Rps.prototype.handleEvents = function() {
    var self = this;
    this.$rock.on('click', function(e) {
        if(self.isPlaying) return;
        self.isPlaying = true;
        var clicked = 0;
        self.rand(clicked);
    });
    this.$paper.on('click', function(e) {
        if(self.isPlaying) return;
        self.isPlaying = true;
        var clicked = 1;
        self.rand(clicked);
    });
    this.$scissors.on('click', function(e) {
        if(self.isPlaying) return;
        self.isPlaying = true;
        var clicked = 2;
        self.rand(clicked);
    });
    $('.handle-btn').children('li').on('mouseover', function(e) {
        $('.handle-btn').children('li').not($(e.currentTarget)).addClass('hide');
    });
     $('.handle-btn').children('li').on('mouseleave', function(e) {
        $('.handle-btn').children('li').not($(e.currentTarget)).removeClass('hide');
    });
    $('.remove').on('click',function() {
        self.removeBlock();
    });
};

Rps.prototype.rand = function(clicked) {
    var num = Math.floor(Math.random() * 3);
    $('.win').removeClass('show');
    $('.lose').removeClass('show');
    $('.draw').removeClass('show');
    this.judge(num,clicked);
};

Rps.prototype.judge = function(num,clicked) {
    var self = this;
    if((num - clicked + 3) % 3 == 2) {
        $('.result').children('li').velocity("fadeOut", { 
            duration: 300, 
            complete:function() {
                $('.draw').filter(':hidden').velocity("fadeIn", { 
                    duration: 300,
                    complete:function() {
                        self.isPlaying = false;
                    } 
                });   
            }
        });
        this.winNumber += 1;
        this.count += 1;
        this.winView(this.winNumber);
    } else if((num - clicked + 3) % 3 == 1)  {
        $('.result').children('li').velocity("fadeOut", { duration: 300, complete:function() {
                $('.lose').filter(':hidden').velocity("fadeIn", { 
                    duration: 300,
                    complete:function() {
                        self.isPlaying = false;
                    } 
                });  
            }
        });
        this.count += 1;
    } else {
        $('.result').children('li').velocity("fadeOut", { duration: 300, complete:function() {
                $('.draw').filter(':hidden').velocity("fadeIn", { 
                    duration: 300,
                    complete:function() {
                        self.isPlaying = false;
                    } 
                });
            }
        });
    }
    if(num == 0) {
        $('.pc').html('<img src="./assets/images/rock.svg" alt="グー" width="180">');
    }else if(num == 1) {
        $('.pc').html('<img src="./assets/images/paper.svg" alt="パー" width="180">');
    }else {
        $('.pc').html('<img src="./assets/images/scissors.svg" alt="チョキ" width="180">');
    }
    $('#count').text(this.count);
    $('#win-num').text(this.winNumber);
    var rate =  Math.round((this.winNumber / this.count) * 100);
    if(this.winNumber == 0) {
        $('#rate-num').text(0);
    }else {
        $('#rate-num').text(rate);
    }
    
};

Rps.prototype.effectFade = function(target,fade) {
    $(target).velocity(fade, { 
        duration: 800
    });
};

Rps.prototype.winView = function(num) {
    var self = this;
    if(num%5 == 0 && num != 0) {
        $('.win-section').velocity({
            left:0
        },{
            duration: 1000
        });
        $('.win-result').text(num);
    }
};

Rps.prototype.removeBlock = function() {
    $('.win-section').velocity({left:-this.wh},{duration:1000});
};

$(function(){
    var rps = new Rps('.rock','.paper','.scissors'); 
});

