/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__scss_style_scss__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__scss_style_scss___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__scss_style_scss__);


var Rps = function(rock,paper,scissors) {
    this.init(rock,paper,scissors);
    this.handleEvents();
};

Rps.prototype.init = function (rock,paper,scissors) {
    this.$rock = $(rock);
    this.$paper = $(paper);
    this.$scissors = $(scissors);
    this.count = 0;
    this.winNumber = 0;
    this.wh = $(window).width(); 
    this.vh = $(window).height();
    this.resizeWindow();
    this.isPlaying = false;
};

Rps.prototype.resizeWindow = function() {
    var timer = false;
    var self = this;
    $(window).resize(function() {
        console.log('test')
        if (timer !== false) {
            clearTimeout(timer);
        }
        timer = setTimeout(function() {
            console.log('resized');
            self.wh = $(window).width();
            self.vh = $(window).height();
        }, 200);
    });
}

Rps.prototype.handleEvents = function() {
    var self = this;
    this.$rock.on('click', function(e) {
        if(self.isPlaying) return;
        self.isPlaying = true;
        var clicked = 0;
        self.rand(clicked);
    });
    this.$paper.on('click', function(e) {
        if(self.isPlaying) return;
        self.isPlaying = true;
        var clicked = 1;
        self.rand(clicked);
    });
    this.$scissors.on('click', function(e) {
        if(self.isPlaying) return;
        self.isPlaying = true;
        var clicked = 2;
        self.rand(clicked);
    });
    $('.handle-btn').children('li').on('mouseover', function(e) {
        $('.handle-btn').children('li').not($(e.currentTarget)).addClass('hide');
    });
     $('.handle-btn').children('li').on('mouseleave', function(e) {
        $('.handle-btn').children('li').not($(e.currentTarget)).removeClass('hide');
    });
    $('.remove').on('click',function() {
        self.removeBlock();
    });
};

Rps.prototype.rand = function(clicked) {
    var num = Math.floor(Math.random() * 3);
    $('.win').removeClass('show');
    $('.lose').removeClass('show');
    $('.draw').removeClass('show');
    this.judge(num,clicked);
};

Rps.prototype.judge = function(num,clicked) {
    var self = this;
    if((num - clicked + 3) % 3 == 2) {
        $('.result').children('li').velocity("fadeOut", { 
            duration: 300, 
            complete:function() {
                $('.draw').filter(':hidden').velocity("fadeIn", { 
                    duration: 300,
                    complete:function() {
                        self.isPlaying = false;
                    } 
                });   
            }
        });
        this.winNumber += 1;
        this.count += 1;
        this.winView(this.winNumber);
    } else if((num - clicked + 3) % 3 == 1)  {
        $('.result').children('li').velocity("fadeOut", { duration: 300, complete:function() {
                $('.lose').filter(':hidden').velocity("fadeIn", { 
                    duration: 300,
                    complete:function() {
                        self.isPlaying = false;
                    } 
                });  
            }
        });
        this.count += 1;
    } else {
        $('.result').children('li').velocity("fadeOut", { duration: 300, complete:function() {
                $('.draw').filter(':hidden').velocity("fadeIn", { 
                    duration: 300,
                    complete:function() {
                        self.isPlaying = false;
                    } 
                });
            }
        });
    }
    if(num == 0) {
        $('.pc').html('<img src="./assets/images/rock.svg" alt="グー" width="180">');
    }else if(num == 1) {
        $('.pc').html('<img src="./assets/images/paper.svg" alt="パー" width="180">');
    }else {
        $('.pc').html('<img src="./assets/images/scissors.svg" alt="チョキ" width="180">');
    }
    $('#count').text(this.count);
    $('#win-num').text(this.winNumber);
    var rate =  Math.round((this.winNumber / this.count) * 100);
    if(this.winNumber == 0) {
        $('#rate-num').text(0);
    }else {
        $('#rate-num').text(rate);
    }
    
};

Rps.prototype.effectFade = function(target,fade) {
    $(target).velocity(fade, { 
        duration: 800
    });
};

Rps.prototype.winView = function(num) {
    var self = this;
    if(num%5 == 0 && num != 0) {
        $('.win-section').velocity({
            left:0
        },{
            duration: 1000
        });
        $('.win-result').text(num);
    }
};

Rps.prototype.removeBlock = function() {
    $('.win-section').velocity({left:-this.wh},{duration:1000});
};

$(function(){
    var rps = new Rps('.rock','.paper','.scissors'); 
});



/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(2);
if(typeof content === 'string') content = [[module.i, content, '']];
// Prepare cssTransformation
var transform;

var options = {}
options.transform = transform
// add the styles to the DOM
var update = __webpack_require__(4)(content, options);
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/lib/loader.js!./style.scss", function() {
			var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/lib/loader.js!./style.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)(undefined);
// imports
exports.push([module.i, "@import url(http://fonts.googleapis.com/earlyaccess/notosansjp.css);", ""]);

// module
exports.push([module.i, "@charset \"UTF-8\";\n/*-------------------------------------------------------*    Variables\n/*-------------------------------------------------------*/\n/*! normalize.css v3.0.1 | MIT License | git.io/normalize */\n/**\n * 1. Set default font family to sans-serif.\n * 2. Prevent iOS text size adjust after orientation change, without disabling\n *    user zoom.\n */\nhtml {\n  font-family: \"Times New Roman\", \"\\6E38\\660E\\671D\", YuMincho, \"Hiragino Mincho ProN\", Meiryo, serif;\n  /* 1 */\n  -ms-text-size-adjust: 100%;\n  /* 2 */\n  -webkit-text-size-adjust: 100%;\n  /* 2 */ }\n\n/**\n * Remove default margin.\n */\nbody {\n  margin: 0; }\n\nh1, h2, h3, h4, h5, h6, p, ul, ol, dl, table, pre {\n  margin: 0; }\n\n/*上方向のmarginを0にします */\ndd {\n  margin-left: 0; }\n\nul {\n  padding-left: 0;\n  margin-bottom: 0; }\n\nli {\n  list-style: none; }\n\n/* HTML5 display definitions\n   ========================================================================== */\n/**\n * Correct `block` display not defined for any HTML5 element in IE 8/9.\n * Correct `block` display not defined for `details` or `summary` in IE 10/11 and Firefox.\n * Correct `block` display not defined for `main` in IE 11.\n */\narticle,\naside,\ndetails,\nfigcaption,\nfigure,\nfooter,\nheader,\nhgroup,\nmain,\nnav,\nsection,\nsummary {\n  display: block; }\n\n/**\n * 1. Correct `inline-block` display not defined in IE 8/9.\n * 2. Normalize vertical alignment of `progress` in Chrome, Firefox, and Opera.\n */\naudio,\ncanvas,\nprogress,\nvideo {\n  display: inline-block;\n  /* 1 */\n  vertical-align: baseline;\n  /* 2 */ }\n\n/**\n * Prevent modern browsers from displaying `audio` without controls.\n * Remove excess height in iOS 5 devices.\n */\naudio:not([controls]) {\n  display: none;\n  height: 0; }\n\n/**\n * Address `[hidden]` styling not present in IE 8/9/10.\n * Hide the `template` element in IE 8/9/11, Safari, and Firefox < 22.\n */\n[hidden],\ntemplate {\n  display: none; }\n\n/* Links\n   ========================================================================== */\n/**\n * Remove the gray background color from active links in IE 10.\n */\na {\n  background: transparent; }\n\n/**\n * Improve readability when focused and also mouse hovered in all browsers.\n */\na:active,\na:hover {\n  outline: 0; }\n\n/* Text-level semantics\n   ========================================================================== */\n/**\n * Address styling not present in IE 8/9/10/11, Safari, and Chrome.\n */\nabbr[title] {\n  border-bottom: 1px dotted; }\n\n/**\n * Address style set to `bolder` in Firefox 4+, Safari, and Chrome.\n */\nb,\nstrong {\n  font-weight: bold; }\n\n/**\n * Address styling not present in Safari and Chrome.\n */\ndfn {\n  font-style: italic; }\n\nhtml {\n  font-size: 75%; }\n\n/* レスポンシブ   タイプセッティングへの対応 */\n/**\n * Address variable `h1` font-size and margin within `section` and `article`\n * contexts in Firefox 4+, Safari, and Chrome.\n */\n/* 禁則処理の追加 */\np, li, dt, dd, th, td, pre {\n  -ms-line-break: strict;\n  line-break: strict;\n  -ms-word-break: break-strict;\n  word-break: break-strict; }\n\n/**\n * Address styling not present in IE 8/9.\n */\nmark {\n  background: #ff0;\n  color: #000; }\n\n/**\n * Address inconsistent and variable font size in all browsers.\n */\nsmall {\n  font-size: 80%; }\n\n/**\n * Prevent `sub` and `sup` affecting `line-height` in all browsers.\n */\nsub,\nsup {\n  font-size: 75%;\n  line-height: 0;\n  position: relative;\n  vertical-align: baseline; }\n\nsup {\n  top: -0.5em; }\n\nsub {\n  bottom: -0.25em; }\n\n/* Embedded content\n   ========================================================================== */\n/**\n * Remove border when inside `a` element in IE 8/9/10.\n */\nimg {\n  border: 0;\n  max-width: 100%;\n  /* フルードイメージへの対応 */\n  vertical-align: middle;\n  /* 追加箇所 */ }\n\n/* IE8 max-widthバグへの対応*/\n/* .ie8 img{width: auto; height: auto} */\n/**\n * Correct overflow not hidden in IE 9/10/11.\n */\nsvg:not(:root) {\n  overflow: hidden; }\n\n/* Grouping content\n   ========================================================================== */\n/**\n * Address margin not present in IE 8/9 and Safari.\n */\nfigure {\n  margin: 1em 40px; }\n\nfigure {\n  margin: 0; }\n\n/**\n * Address differences between Firefox and other browsers.\n */\nhr {\n  -moz-box-sizing: content-box;\n  box-sizing: content-box;\n  height: 0; }\n\n/**\n * Contain overflow in all browsers.\n */\npre {\n  overflow: auto; }\n\n/**\n * Address odd `em`-unit font size rendering in all browsers.\n */\ncode,\nkbd,\npre,\nsamp {\n  font-family: monospace, monospace;\n  font-size: 1em; }\n\n/* Forms\n   ========================================================================== */\n/**\n * Known limitation: by default, Chrome and Safari on OS X allow very limited\n * styling of `select`, unless a `border` property is set.\n */\n/**\n * 1. Correct color not being inherited.\n *    Known issue: affects color of disabled elements.\n * 2. Correct font properties not being inherited.\n * 3. Address margins set differently in Firefox 4+, Safari, and Chrome.\n */\nbutton,\ninput,\noptgroup,\nselect,\ntextarea {\n  color: inherit;\n  /* 1 */\n  font: inherit;\n  /* 2 */\n  margin: 0;\n  /* 3 */ }\n\n/**\n * Address `overflow` set to `hidden` in IE 8/9/10/11.\n */\nbutton {\n  overflow: visible; }\n\n/**\n * Address inconsistent `text-transform` inheritance for `button` and `select`.\n * All other form control elements do not inherit `text-transform` values.\n * Correct `button` style inheritance in Firefox, IE 8/9/10/11, and Opera.\n * Correct `select` style inheritance in Firefox.\n */\nbutton,\nselect {\n  text-transform: none; }\n\n/**\n * 1. Avoid the WebKit bug in Android 4.0.* where (2) destroys native `audio`\n *    and `video` controls.\n * 2. Correct inability to style clickable `input` types in iOS.\n * 3. Improve usability and consistency of cursor style between image-type\n *    `input` and others.\n */\nbutton,\nhtml input[type=\"button\"],\ninput[type=\"reset\"],\ninput[type=\"submit\"] {\n  -webkit-appearance: button;\n  /* 2 */\n  cursor: pointer;\n  /* 3 */ }\n\n/**\n * Re-set default cursor for disabled elements.\n */\nbutton[disabled],\nhtml input[disabled] {\n  cursor: default; }\n\n/**\n * Remove inner padding and border in Firefox 4+.\n */\nbutton::-moz-focus-inner,\ninput::-moz-focus-inner {\n  border: 0;\n  padding: 0; }\n\n/**\n * Address Firefox 4+ setting `line-height` on `input` using `!important` in\n * the UA stylesheet.\n */\ninput {\n  line-height: normal; }\n\n/**\n * It's recommended that you don't attempt to style these elements.\n * Firefox's implementation doesn't respect box-sizing, padding, or width.\n *\n * 1. Address box sizing set to `content-box` in IE 8/9/10.\n * 2. Remove excess padding in IE 8/9/10.\n */\ninput[type=\"checkbox\"],\ninput[type=\"radio\"] {\n  box-sizing: border-box;\n  /* 1 */\n  padding: 0;\n  /* 2 */ }\n\n/**\n * Fix the cursor style for Chrome's increment/decrement buttons. For certain\n * `font-size` values of the `input`, it causes the cursor style of the\n * decrement button to change from `default` to `text`.\n */\ninput[type=\"number\"]::-webkit-inner-spin-button,\ninput[type=\"number\"]::-webkit-outer-spin-button {\n  height: auto; }\n\n/**\n * 1. Address `appearance` set to `searchfield` in Safari and Chrome.\n * 2. Address `box-sizing` set to `border-box` in Safari and Chrome\n *    (include `-moz` to future-proof).\n */\ninput[type=\"search\"] {\n  -webkit-appearance: textfield;\n  /* 1 */\n  -moz-box-sizing: content-box;\n  -webkit-box-sizing: content-box;\n  /* 2 */\n  box-sizing: content-box; }\n\n/**\n * Remove inner padding and search cancel button in Safari and Chrome on OS X.\n * Safari (but not Chrome) clips the cancel button when the search input has\n * padding (and `textfield` appearance).\n */\ninput[type=\"search\"]::-webkit-search-cancel-button,\ninput[type=\"search\"]::-webkit-search-decoration {\n  -webkit-appearance: none; }\n\n/**\n * Define consistent border, margin, and padding.\n */\nfieldset {\n  border: 1px solid #c0c0c0;\n  margin: 0 2px;\n  padding: 0.35em 0.625em 0.75em; }\n\n/**\n * 1. Correct `color` not being inherited in IE 8/9/10/11.\n * 2. Remove padding so people aren't caught out if they zero out fieldsets.\n */\nlegend {\n  border: 0;\n  /* 1 */\n  padding: 0;\n  /* 2 */ }\n\n/**\n * Remove default vertical scrollbar in IE 8/9/10/11.\n */\ntextarea {\n  overflow: auto; }\n\n/**\n * Don't inherit the `font-weight` (applied by a rule above).\n * NOTE: the default cannot safely be changed in Chrome and Safari on OS X.\n */\noptgroup {\n  font-weight: bold; }\n\n/* Tables\n   ========================================================================== */\n/**\n * Remove most spacing between table cells.\n */\ntable {\n  border-collapse: collapse;\n  border-spacing: 0; }\n\ntd,\nth {\n  padding: 0; }\n\n/* --------------------------------------------------------------------------------------\n FLOAT CLEAR\n--------------------------------------------------------------------------------------- */\n/* Clearfix */\n.clearfix:after {\n  content: \".\";\n  display: block;\n  height: 0;\n  clear: both;\n  visibility: hidden;\n  font-size: 0.1em;\n  line-height: 0; }\n\n.clearfix {\n  display: inline-block;\n  min-height: 1%; }\n\n/* Hides from IE-mac \\*/\n* html .clearfix {\n  height: 1%; }\n\n.clearfix {\n  display: block; }\n\n/* End hide from IE-Mac */\n/*-------------------------------------------------------*    Base\n/*-------------------------------------------------------*/\nbody {\n  background: #fff;\n  word-wrap: break-word; }\n\nhtml, body {\n  height: 100%; }\n\nimg {\n  max-width: 100%;\n  vertical-align: middle; }\n\na {\n  word-break: break-all; }\n\n/*-------------------------------------------------------*    Typography\n/*-------------------------------------------------------*/\nhtml {\n  font-family: \"Lato\", \"Hiragino Kaku Gothic ProN\", \"\\6E38\\30B4\\30B7\\30C3\\30AF\", YuGothic, Meiryo, sans-serif;\n  line-height: 1.7;\n  font-size: 14px;\n  font-weight: 300; }\n\n@media screen and (min-width: 768px) {\n  html {\n    font-size: 14px; } }\n\n@media screen and (min-width: 1024px) {\n  html {\n    font-size: 16px; } }\n\n.gothic {\n  font-family: \"Noto Sans JP\", \"Lato\", \"Hiragino Kaku Gothic ProN\", \"\\6E38\\30B4\\30B7\\30C3\\30AF\", YuGothic, Meiryo, sans-serif; }\n\n.mincho {\n  font-family: \"Times New Roman\", \"\\6E38\\660E\\671D\", YuMincho, \"Hiragino Mincho ProN\", Meiryo, serif; }\n\n/* --------------------------------------------------------------------------------------\n HEAD LINE \n--------------------------------------------------------------------------------------- */\nh1, h2, h3, h4, h5, h6 {\n  margin-bottom: 0.5em;\n  font-weight: normal; }\n\nh1 {\n  font-size: 1.5em;\n  line-height: 1.5; }\n\nh2 {\n  font-size: 1.3333em;\n  line-height: 1.3333; }\n\nh3 {\n  font-size: 1.25em;\n  line-height: 1.3333; }\n\nh4, h5, h6 {\n  font-size: 1.15em;\n  line-height: 1.5; }\n\n/*\nh1 {\n\n    font-size: 33px;\n    font-size: 3.3rem;\n    line-height: 1.273;\n    margin-top: 42px;\n    margin-top: 4.2rem;\n    margin-bottom: 21px;\n    margin-bottom: 2.1rem;\n\n    @include break-at(640px) {\n\n        font-size: 54px;\n        font-size: 5.4rem;\n        line-height: 1.333;\n        margin-top: 48px;\n        margin-top: 4.8rem;\n        margin-bottom: 24px;\n        margin-bottom: 2.4rem;\n\n    }\n\n    @include break-at(1200px){\n\n        font-size: 64px;\n        font-size: 6.4rem;\n        line-height: 1.406; \n        margin-top: 60px;\n        margin-top: 6rem;\n        margin-bottom: 30px;\n        margin-bottom: 3rem;\n\n    }\n\n}\n\nh2 {\n\n    font-size: 25px;\n    font-size: 2.5rem;\n    line-height: 1.680;\n    margin-top: 42px;\n    margin-top: 4.2rem;\n    margin-bottom: 21px;\n    margin-bottom: 2.1rem;\n\n    @include break-at(640px) {\n\n        font-size: 36px;\n        font-size: 3.6rem;\n        line-height: 1.333;\n        margin-top: 48px;\n        margin-top: 4.8rem;\n        margin-bottom: 24px;\n        margin-bottom: 2.4rem;\n\n    }\n\n    @include break-at(1200px) {\n\n        font-size: 43px;\n        font-size: 4.3rem;\n        line-height: 1.395;\n        margin-top: 60px;\n        margin-top: 6rem;\n        margin-bottom: 30px;\n        margin-bottom: 3rem;\n\n    }\n\n}\n\nh3 {\n\n    font-size: 19px;\n    font-size: 1.9rem;\n    line-height: 1.105; \n    margin-top: 21px;\n    margin-top: 2.1rem;\n    margin-bottom: 21px;\n    margin-bottom: 2.1rem;\n\n    @include break-at(640px) {\n\n        font-size: 24px;\n        font-size: 2.4rem;\n        line-height: 1; \n        margin-top: 24px;\n        margin-top: 2.4rem;\n        margin-bottom: 24px;\n        margin-bottom: 2.4rem;\n\n    }\n\n    @include break-at(1200px) {\n\n        font-size: 29px;\n        font-size: 2.9rem;\n        line-height: 1.034;\n        margin-top: 30px;\n        margin-top: 3rem;\n        margin-bottom: 30px;\n        margin-bottom: 3rem;\n\n    }\n}\n\nh4 {\n\n    font-size: 19px;\n    font-size: 1.9rem;\n    line-height: 1.105;\n    margin-top: 21px;\n    margin-top: 2.1rem;\n    margin-bottom: 21px;\n    margin-bottom: 2.1rem;\n\n    @include break-at(640px) {\n\n        font-size: 19px;\n        font-size: 1.9rem;\n        line-height: 1.105; \n        margin-top: 21px;\n        margin-top: 2.1rem;\n        margin-bottom: 21px;\n        margin-bottom: 2.1rem;\n\n    }\n\n    @include break-at(1200px) {\n\n        font-size: 24px;\n        font-size: 2.4rem;\n        line-height: 24px;\n        line-height: 2.4rem;\n        margin-top: 24px;\n        margin-top: 2.4rem;\n        margin-bottom: 24px;\n        margin-bottom: 2.4rem;\n\n    }\n\n}\n\nh5 {\n\n    font-size: 19px;\n    font-size: 1.9rem;\n    line-height: 1.105; \n    margin-top: 21px;\n    margin-top: 2.1rem;\n    margin-bottom: 21px;\n    margin-bottom: 2.1rem;\n\n    @include break-at(640px) {\n\n        font-size: 19px;\n        font-size: 1.9rem;\n        line-height: 1.105; \n        margin-top: 21px;\n        margin-top: 2.1rem;\n        margin-bottom: 21px;\n        margin-bottom: 2.1rem;\n\n    }\n\n    @include break-at(1200px) {\n\n        font-size: 24px;\n        font-size: 2.4rem;\n        line-height: 1; \n        margin-top: 24px;\n        margin-top: 2.4rem;\n        margin-bottom: 24px;\n        margin-bottom: 2.4rem;\n\n    }\n\n}\n\n\nh6 {\n\n    font-size: 19px;\n    font-size: 1.9rem;\n    line-height: 1.105; \n    margin-top: 21px;\n    margin-top: 2.1rem;\n    margin-bottom: 21px;\n    margin-bottom: 2.1rem;\n\n    @include break-at(640px) {\n\n        font-size: 19px;\n        font-size: 1.9rem;\n        line-height: 1.105; \n        margin-top: 21px;\n        margin-top: 2.1rem;\n        margin-bottom: 21px;\n        margin-bottom: 2.1rem;\n\n    }\n\n    @include break-at(1200px) {\n\n        font-size: 24px;\n        font-size: 2.4rem;\n        line-height: 1; \n        margin-top: 24px;\n        margin-top: 2.4rem;\n        margin-bottom: 24px;\n        margin-bottom: 2.4rem;\n\n    }\n\n}\n\np, ul, ol, pre, table, blockquote {\n\n    margin-top: 21px;\n    margin-top: 2.1rem;\n    margin-bottom: 21px;\n    margin-bottom: 2.1rem;\n\n    @include break-at(640px) {\n\n        margin-top: 24px;\n        margin-top: 2.4rem;\n        margin-bottom: 24px;\n        margin-bottom: 2.4rem;\n\n    }\n\n    @include break-at(1000px) {\n\n        margin-top: 30px;\n        margin-top: 3rem;\n        margin-bottom: 30px;\n        margin-bottom: 3rem;\n\n    }\n\n}\n\np, ul, ol, pre, table, blockquote {\n\n    margin-top: 21px;\n    margin-top: 2.1rem;\n    margin-bottom: 21px;\n    margin-bottom: 2.1rem;\n\n}    \n\n\nhr {\n\n    border: 0;\n    height: 0;\n    border-top: 0;\n    border-bottom: 1px solid rgba(233, 233, 233, 1);\n\n}\nul ul, ol ol, ul ol, ol ul {\n\n    margin-top: 0;\n    margin-bottom: 0;\n\n}\nb, strong, em, small, code {\n\n    line-height: 1;\n\n}\nsup, sub {\n\n    vertical-align: baseline;\n    position: relative;\n    top: -0.4em;\n\n}\nsub {\n\n    top: 0.4em;\n\n}\n*/\n.win, .lose, .draw {\n  display: none; }\n\n.show {\n  display: block; }\n\n.hide {\n  opacity: 0.4;\n  transition: all 0.3s; }\n\n.first-view {\n  background-color: #fcff00;\n  height: 100vh;\n  width: 100%;\n  text-align: center;\n  display: table; }\n\n.handle-btn {\n  max-width: 700px;\n  margin: 0 auto;\n  display: flex;\n  flex-wrap: wrap;\n  align-items: center;\n  justify-content: center; }\n  .handle-btn li {\n    width: 33.3333%;\n    cursor: pointer;\n    box-sizing: border-box; }\n    .handle-btn li img {\n      padding: 2em; }\n\n.inner {\n  display: table-cell;\n  max-width: 1040px;\n  margin: 0 auto;\n  padding: 4%;\n  vertical-align: middle; }\n\nh1.title {\n  font-size: 3rem;\n  position: fixed;\n  top: 0;\n  left: 0;\n  height: 56px;\n  width: 100%;\n  background: #fff;\n  display: flex;\n  align-items: center;\n  justify-content: center; }\n  h1.title img {\n    height: 32px;\n    padding: 0 4%; }\n\n.scores {\n  position: fixed;\n  bottom: 1em;\n  right: 1em;\n  width: 100%;\n  text-align: right; }\n  .scores dt, .scores dd {\n    display: inline-block; }\n  .scores dt {\n    font-weight: bold; }\n\n.show {\n  display: block;\n  opacity: 1;\n  animation-duration: 0.8s;\n  animation-name: fade-in;\n  -moz-animation-duration: 0.8s;\n  -moz-animation-name: fade-in;\n  -webkit-animation-duration: 0.8s;\n  -webkit-animation-name: fade-in; }\n\n.result {\n  height: 15vh;\n  margin-bottom: 4vh; }\n\n.pc {\n  margin-bottom: 4vh; }\n\n.win-section {\n  width: 100%;\n  height: 100%;\n  background: #e21e9f;\n  position: absolute;\n  top: 0;\n  left: -100%;\n  display: flex;\n  align-items: center; }\n  .win-section .sec-inner {\n    width: 100%; }\n    .win-section .sec-inner h1 {\n      text-align: center;\n      color: #fff; }\n\n.remove {\n  position: absolute;\n  right: 1rem;\n  bottom: 0; }\n  .remove::before {\n    content: \"\\F00D\";\n    font-family: 'FontAwesome';\n    font-size: 3rem;\n    color: #fff; }\n\ndiv.wrap {\n  position: relative; }\n\n.win-result {\n  font-size: 8rem;\n  font-weight: bold; }\n", ""]);

// exports


/***/ }),
/* 3 */
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function(useSourceMap) {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		return this.map(function (item) {
			var content = cssWithMappingToString(item, useSourceMap);
			if(item[2]) {
				return "@media " + item[2] + "{" + content + "}";
			} else {
				return content;
			}
		}).join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};

function cssWithMappingToString(item, useSourceMap) {
	var content = item[1] || '';
	var cssMapping = item[3];
	if (!cssMapping) {
		return content;
	}

	if (useSourceMap && typeof btoa === 'function') {
		var sourceMapping = toComment(cssMapping);
		var sourceURLs = cssMapping.sources.map(function (source) {
			return '/*# sourceURL=' + cssMapping.sourceRoot + source + ' */'
		});

		return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
	}

	return [content].join('\n');
}

// Adapted from convert-source-map (MIT)
function toComment(sourceMap) {
	// eslint-disable-next-line no-undef
	var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
	var data = 'sourceMappingURL=data:application/json;charset=utf-8;base64,' + base64;

	return '/*# ' + data + ' */';
}


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/

var stylesInDom = {};

var	memoize = function (fn) {
	var memo;

	return function () {
		if (typeof memo === "undefined") memo = fn.apply(this, arguments);
		return memo;
	};
};

var isOldIE = memoize(function () {
	// Test for IE <= 9 as proposed by Browserhacks
	// @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
	// Tests for existence of standard globals is to allow style-loader
	// to operate correctly into non-standard environments
	// @see https://github.com/webpack-contrib/style-loader/issues/177
	return window && document && document.all && !window.atob;
});

var getElement = (function (fn) {
	var memo = {};

	return function(selector) {
		if (typeof memo[selector] === "undefined") {
			memo[selector] = fn.call(this, selector);
		}

		return memo[selector]
	};
})(function (target) {
	return document.querySelector(target)
});

var singleton = null;
var	singletonCounter = 0;
var	stylesInsertedAtTop = [];

var	fixUrls = __webpack_require__(5);

module.exports = function(list, options) {
	if (typeof DEBUG !== "undefined" && DEBUG) {
		if (typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
	}

	options = options || {};

	options.attrs = typeof options.attrs === "object" ? options.attrs : {};

	// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
	// tags it will allow on a page
	if (!options.singleton) options.singleton = isOldIE();

	// By default, add <style> tags to the <head> element
	if (!options.insertInto) options.insertInto = "head";

	// By default, add <style> tags to the bottom of the target
	if (!options.insertAt) options.insertAt = "bottom";

	var styles = listToStyles(list, options);

	addStylesToDom(styles, options);

	return function update (newList) {
		var mayRemove = [];

		for (var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];

			domStyle.refs--;
			mayRemove.push(domStyle);
		}

		if(newList) {
			var newStyles = listToStyles(newList, options);
			addStylesToDom(newStyles, options);
		}

		for (var i = 0; i < mayRemove.length; i++) {
			var domStyle = mayRemove[i];

			if(domStyle.refs === 0) {
				for (var j = 0; j < domStyle.parts.length; j++) domStyle.parts[j]();

				delete stylesInDom[domStyle.id];
			}
		}
	};
};

function addStylesToDom (styles, options) {
	for (var i = 0; i < styles.length; i++) {
		var item = styles[i];
		var domStyle = stylesInDom[item.id];

		if(domStyle) {
			domStyle.refs++;

			for(var j = 0; j < domStyle.parts.length; j++) {
				domStyle.parts[j](item.parts[j]);
			}

			for(; j < item.parts.length; j++) {
				domStyle.parts.push(addStyle(item.parts[j], options));
			}
		} else {
			var parts = [];

			for(var j = 0; j < item.parts.length; j++) {
				parts.push(addStyle(item.parts[j], options));
			}

			stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
		}
	}
}

function listToStyles (list, options) {
	var styles = [];
	var newStyles = {};

	for (var i = 0; i < list.length; i++) {
		var item = list[i];
		var id = options.base ? item[0] + options.base : item[0];
		var css = item[1];
		var media = item[2];
		var sourceMap = item[3];
		var part = {css: css, media: media, sourceMap: sourceMap};

		if(!newStyles[id]) styles.push(newStyles[id] = {id: id, parts: [part]});
		else newStyles[id].parts.push(part);
	}

	return styles;
}

function insertStyleElement (options, style) {
	var target = getElement(options.insertInto)

	if (!target) {
		throw new Error("Couldn't find a style target. This probably means that the value for the 'insertInto' parameter is invalid.");
	}

	var lastStyleElementInsertedAtTop = stylesInsertedAtTop[stylesInsertedAtTop.length - 1];

	if (options.insertAt === "top") {
		if (!lastStyleElementInsertedAtTop) {
			target.insertBefore(style, target.firstChild);
		} else if (lastStyleElementInsertedAtTop.nextSibling) {
			target.insertBefore(style, lastStyleElementInsertedAtTop.nextSibling);
		} else {
			target.appendChild(style);
		}
		stylesInsertedAtTop.push(style);
	} else if (options.insertAt === "bottom") {
		target.appendChild(style);
	} else {
		throw new Error("Invalid value for parameter 'insertAt'. Must be 'top' or 'bottom'.");
	}
}

function removeStyleElement (style) {
	if (style.parentNode === null) return false;
	style.parentNode.removeChild(style);

	var idx = stylesInsertedAtTop.indexOf(style);
	if(idx >= 0) {
		stylesInsertedAtTop.splice(idx, 1);
	}
}

function createStyleElement (options) {
	var style = document.createElement("style");

	options.attrs.type = "text/css";

	addAttrs(style, options.attrs);
	insertStyleElement(options, style);

	return style;
}

function createLinkElement (options) {
	var link = document.createElement("link");

	options.attrs.type = "text/css";
	options.attrs.rel = "stylesheet";

	addAttrs(link, options.attrs);
	insertStyleElement(options, link);

	return link;
}

function addAttrs (el, attrs) {
	Object.keys(attrs).forEach(function (key) {
		el.setAttribute(key, attrs[key]);
	});
}

function addStyle (obj, options) {
	var style, update, remove, result;

	// If a transform function was defined, run it on the css
	if (options.transform && obj.css) {
	    result = options.transform(obj.css);

	    if (result) {
	    	// If transform returns a value, use that instead of the original css.
	    	// This allows running runtime transformations on the css.
	    	obj.css = result;
	    } else {
	    	// If the transform function returns a falsy value, don't add this css.
	    	// This allows conditional loading of css
	    	return function() {
	    		// noop
	    	};
	    }
	}

	if (options.singleton) {
		var styleIndex = singletonCounter++;

		style = singleton || (singleton = createStyleElement(options));

		update = applyToSingletonTag.bind(null, style, styleIndex, false);
		remove = applyToSingletonTag.bind(null, style, styleIndex, true);

	} else if (
		obj.sourceMap &&
		typeof URL === "function" &&
		typeof URL.createObjectURL === "function" &&
		typeof URL.revokeObjectURL === "function" &&
		typeof Blob === "function" &&
		typeof btoa === "function"
	) {
		style = createLinkElement(options);
		update = updateLink.bind(null, style, options);
		remove = function () {
			removeStyleElement(style);

			if(style.href) URL.revokeObjectURL(style.href);
		};
	} else {
		style = createStyleElement(options);
		update = applyToTag.bind(null, style);
		remove = function () {
			removeStyleElement(style);
		};
	}

	update(obj);

	return function updateStyle (newObj) {
		if (newObj) {
			if (
				newObj.css === obj.css &&
				newObj.media === obj.media &&
				newObj.sourceMap === obj.sourceMap
			) {
				return;
			}

			update(obj = newObj);
		} else {
			remove();
		}
	};
}

var replaceText = (function () {
	var textStore = [];

	return function (index, replacement) {
		textStore[index] = replacement;

		return textStore.filter(Boolean).join('\n');
	};
})();

function applyToSingletonTag (style, index, remove, obj) {
	var css = remove ? "" : obj.css;

	if (style.styleSheet) {
		style.styleSheet.cssText = replaceText(index, css);
	} else {
		var cssNode = document.createTextNode(css);
		var childNodes = style.childNodes;

		if (childNodes[index]) style.removeChild(childNodes[index]);

		if (childNodes.length) {
			style.insertBefore(cssNode, childNodes[index]);
		} else {
			style.appendChild(cssNode);
		}
	}
}

function applyToTag (style, obj) {
	var css = obj.css;
	var media = obj.media;

	if(media) {
		style.setAttribute("media", media)
	}

	if(style.styleSheet) {
		style.styleSheet.cssText = css;
	} else {
		while(style.firstChild) {
			style.removeChild(style.firstChild);
		}

		style.appendChild(document.createTextNode(css));
	}
}

function updateLink (link, options, obj) {
	var css = obj.css;
	var sourceMap = obj.sourceMap;

	/*
		If convertToAbsoluteUrls isn't defined, but sourcemaps are enabled
		and there is no publicPath defined then lets turn convertToAbsoluteUrls
		on by default.  Otherwise default to the convertToAbsoluteUrls option
		directly
	*/
	var autoFixUrls = options.convertToAbsoluteUrls === undefined && sourceMap;

	if (options.convertToAbsoluteUrls || autoFixUrls) {
		css = fixUrls(css);
	}

	if (sourceMap) {
		// http://stackoverflow.com/a/26603875
		css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
	}

	var blob = new Blob([css], { type: "text/css" });

	var oldSrc = link.href;

	link.href = URL.createObjectURL(blob);

	if(oldSrc) URL.revokeObjectURL(oldSrc);
}


/***/ }),
/* 5 */
/***/ (function(module, exports) {


/**
 * When source maps are enabled, `style-loader` uses a link element with a data-uri to
 * embed the css on the page. This breaks all relative urls because now they are relative to a
 * bundle instead of the current page.
 *
 * One solution is to only use full urls, but that may be impossible.
 *
 * Instead, this function "fixes" the relative urls to be absolute according to the current page location.
 *
 * A rudimentary test suite is located at `test/fixUrls.js` and can be run via the `npm test` command.
 *
 */

module.exports = function (css) {
  // get current location
  var location = typeof window !== "undefined" && window.location;

  if (!location) {
    throw new Error("fixUrls requires window.location");
  }

	// blank or null?
	if (!css || typeof css !== "string") {
	  return css;
  }

  var baseUrl = location.protocol + "//" + location.host;
  var currentDir = baseUrl + location.pathname.replace(/\/[^\/]*$/, "/");

	// convert each url(...)
	/*
	This regular expression is just a way to recursively match brackets within
	a string.

	 /url\s*\(  = Match on the word "url" with any whitespace after it and then a parens
	   (  = Start a capturing group
	     (?:  = Start a non-capturing group
	         [^)(]  = Match anything that isn't a parentheses
	         |  = OR
	         \(  = Match a start parentheses
	             (?:  = Start another non-capturing groups
	                 [^)(]+  = Match anything that isn't a parentheses
	                 |  = OR
	                 \(  = Match a start parentheses
	                     [^)(]*  = Match anything that isn't a parentheses
	                 \)  = Match a end parentheses
	             )  = End Group
              *\) = Match anything and then a close parens
          )  = Close non-capturing group
          *  = Match anything
       )  = Close capturing group
	 \)  = Match a close parens

	 /gi  = Get all matches, not the first.  Be case insensitive.
	 */
	var fixedCss = css.replace(/url\s*\(((?:[^)(]|\((?:[^)(]+|\([^)(]*\))*\))*)\)/gi, function(fullMatch, origUrl) {
		// strip quotes (if they exist)
		var unquotedOrigUrl = origUrl
			.trim()
			.replace(/^"(.*)"$/, function(o, $1){ return $1; })
			.replace(/^'(.*)'$/, function(o, $1){ return $1; });

		// already a full url? no change
		if (/^(#|data:|http:\/\/|https:\/\/|file:\/\/\/)/i.test(unquotedOrigUrl)) {
		  return fullMatch;
		}

		// convert the url to a full url
		var newUrl;

		if (unquotedOrigUrl.indexOf("//") === 0) {
		  	//TODO: should we add protocol?
			newUrl = unquotedOrigUrl;
		} else if (unquotedOrigUrl.indexOf("/") === 0) {
			// path should be relative to the base url
			newUrl = baseUrl + unquotedOrigUrl; // already starts with '/'
		} else {
			// path should be relative to current directory
			newUrl = currentDir + unquotedOrigUrl.replace(/^\.\//, ""); // Strip leading './'
		}

		// send back the fixed url(...)
		return "url(" + JSON.stringify(newUrl) + ")";
	});

	// send back the fixed css
	return fixedCss;
};


/***/ })
/******/ ]);