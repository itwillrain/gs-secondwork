module.exports = {
  // メインとなるJavaScriptファイル（エントリーポイント）
  entry: `./assets/js/main.js`,
  // ファイルの出力設定
  output : {
    //  出力ファイルのディレクトリ名
    path     : `${__dirname}/`,
    // 出力ファイル名
    filename : 'bundle.js'
  },
  module: {
    // Loaderの設定
    rules: [
      // Sassファイルの読み込みとコンパイル
      {
        test: /\.scss/, // 対象となるファイルの拡張子
        loaders: ['style-loader', 'css-loader', 'sass-loader']
      }
    ]
  }
};
